import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import router from './router';
// import './utils/index';
import uuid from 'vue-uuid';
import { DatePicker } from 'ant-design-vue';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

// createApp(App).mount('#app');
// 5. 创建并挂载根实例
const app = createApp(App);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(DatePicker);
app.use(router);
app.use(uuid);
app.mount('#app');
