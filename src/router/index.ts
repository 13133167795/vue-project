import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';


const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('../views/index.vue'),
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/home/home.vue'),
      },
      {
        path: '/prod-prodTag',
        name: 'prodTag',
        component: () => import('../views/product/prodTag.vue'),
      },
      {
        path: '/prod-prodList',
        name: 'prodList',
        component: () => import('../views/product/prodList.vue'),
      },
      {
        path: '/prod-category',
        name: 'category',
        component: () => import('../views/product/category.vue'),
      },
      {
        path: '/prod-prodComm',
        name: 'prodComm',
        component: () => import('../views/product/prodComm.vue'),
      },
      {
        path: '/prod-spec',
        name: 'spec',
        component: () => import('../views/product/spec.vue'),
      },
      {
        path: '/shop-notice',
        name: 'notice',
        component: () => import('../views/shop/notice.vue'),
      },
      {
        path: '/shop-hotSearch',
        name: 'hotSearch',
        component: () => import('../views/shop/hotSearch.vue'),
      },
      {
        path: '/admin-indexImg',
        name: 'indexImg',
        component: () => import('../views/shop/indexImg.vue'),
      },
      {
        path: '/shop-transport',
        name: 'transport',
        component: () => import('../views/shop/transport.vue'),
      },
      {
        path: '/shop-pickAddr',
        name: 'pickAddr',
        component: () => import('../views/shop/pickAddr.vue'),
      },
      {
        path: '/vip-user',
        name: 'vip',
        component: () => import('../views/vip/vip.vue'),
      },
      {
        path: '/order-order',
        name: 'order',
        component: () => import('../views/order/order.vue'),
      },
      {
        path: '/sys-area',
        name: 'area',
        component: () => import('../views/system/area.vue'),
      },
      {
        path: '/sys-user',
        name: 'user',
        component: () => import('../views/system/user.vue'),
      },
      {
        path: '/sys-role',
        name: 'role',
        component: () => import('../views/system/role.vue'),
      },
      {
        path: '/sys-menu',
        name: 'menu',
        component: () => import('../views/system/menu.vue'),
      },
      {
        path: '/sys-schedule',
        name: 'schedule',
        component: () => import('../views/system/schedule.vue'),
      },
      {
        path: '/sys-config',
        name: 'config',
        component: () => import('../views/system/config.vue'),
      },
      {
        path: '/sys-log',
        name: 'log',
        component: () => import('../views/system/log.vue'),
      },
    ],
  },
  { path: '/login', component: () => import('@/views/login/login.vue') },
];

const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
});

export default router;
