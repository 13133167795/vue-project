// 第一种

// 开发环境

// export const base_url = 'https://bjwz.bwie.com/mall4w/dev';
// export const base_name = '2001';

// 生产环境
// export const base_url = 'https://bjwz.bwie.com/mall4w/pro';
// export const base_name = '2001';

// 测试环境
// export const base_url = 'https://bjwz.bwie.com/mall4w/';
// export const base_name = '2001';

// 第二种

// console.log(process.env.NODE_ENV);
// let base_url = '';
// let base_name = '';

// if (process.env.NODE_ENV === 'develpoment') {
//   base_url = 'https://bjwz.bwie.com/mall4w/dev';
//   base_name = '2001';
// } else if (process.env.NODE_ENV === 'prodoction') {
//   base_url = 'https://bjwz.bwie.com/mall4w/pro';
//   base_name = '2001';
// } else if (process.env.NODE_ENV === 'test') {
//   base_url = 'https://bjwz.bwie.com/mall4w/test';
//   base_name = '2001';
// }

// export { base_url, base_name };

// 第3种
// .env
// .env.production
// .env.development

// export const base_url='https://bjwz.bwie.com/mall4w/dev'
// export const base_name="2001A"

// 生产环境
// export const base_url='https://bjwz.bwie.com/mall4w/pro'
// export const base_name="2001A"

// 测试环境
// export const base_url='https://bjwz.bwie.com/mall4w/test'
// export const base_name="2001A"

// 第二种
// console.log(process.env.NODE_ENV);
// let base_name='';
// let base_url='';

// if(process.env.NODE_ENV==='develpoment'){//开发
//      base_url='https://bjwz.bwie.com/mall4w/dev'
//      base_name="2001A"
// }
// else if(process.env.NODE_ENV==='prodoction'){//生产
//     base_url='https://bjwz.bwie.com/mall4w/pro'
//     base_name="2001A"
// }
// else if(process.env.NODE_ENV==='test'){//预发布
//     base_url='https://bjwz.bwie.com/mall4w/test'
//     base_name="2001A"
// }

// export { base_url,base_name }
// 第三种

// .env
// .env.prodoction
// .env.develpoment
export {};
